FROM alpine:latest
MAINTAINER Marek Rychly <marek.rychly@gmail.com>

ARG ROUNDCUBEMAIL_VERSION=1.3.6
ARG ROUNDCUBEMAIL_HTTPS=0
ARG ROUNDCUBEMAIL_HOME=/home/roundcubemail

COPY bin /usr/local/bin
COPY home ${ROUNDCUBEMAIL_HOME}

RUN \
apk --no-cache add lighttpd php7-fpm \
php7-dom php7-session php7-xml php7-json php7-pdo_sqlite php7-mbstring php7-openssl php7-zip \
php7-fileinfo php7-iconv php7-intl php7-exif php7-ldap php7-gd \
gnupg && \
sed -i \
-e 's|^#\s*\("mod_setenv",\)$|\1|' \
-e 's|^#\s*\("mod_compress",\)$|\1|' \
-e 's|^#\s*\(include "mod_fastcgi_fpm.conf"\)$|\1|' \
/etc/lighttpd/lighttpd.conf && \
sed -i \
-e 's|^;\s*\(date\.timezone\s*=\).*$|\1"Europe/Prague"|' \
-e "s|^;\\s*\\(openssl\\.cafile\\s*=\).*\$|\\1\"${ROUNDCUBEMAIL_HOME}/roundcubemail-host-certs.pem\"|" \
/etc/php7/php.ini && \
sed -i \
-e 's|^;\s*\(catch_workers_output\s*=\).*$|\1true|' \
/etc/php7/php-fpm.conf && \
chmod 755 /usr/local/bin/* && \
mkdir -p /opt ${ROUNDCUBEMAIL_HOME}/roundcubemail-enigma && \
wget "https://github.com/roundcube/roundcubemail/releases/download/${ROUNDCUBEMAIL_VERSION}/roundcubemail-${ROUNDCUBEMAIL_VERSION}-complete.tar.gz" -O - | tar -xzC /opt && \
ln -vs roundcubemail-${ROUNDCUBEMAIL_VERSION} /opt/roundcubemail && \
ln -vs ${ROUNDCUBEMAIL_HOME}/roundcubemail-config.inc.php /opt/roundcubemail/config/config.inc.php && \
rm -r /var/www/localhost/htdocs && ln -vs /opt/roundcubemail /var/www/localhost/htdocs && \
sed -i \
-e "s|/home/roundcubemail/|${ROUNDCUBEMAIL_HOME}/|" \
${ROUNDCUBEMAIL_HOME}/roundcubemail-config.inc.php && \
rm -r /opt/roundcubemail/plugins/enigma/home && ln -vs ${ROUNDCUBEMAIL_HOME}/roundcubemail-enigma /opt/roundcubemail/plugins/enigma/home && \
chown -R nobody:nobody ${ROUNDCUBEMAIL_HOME} ${ROUNDCUBEMAIL_HOME}/roundcubemail-enigma

RUN \
if [ "${ROUNDCUBEMAIL_HTTPS}" -eq 1 ]; then \
sed -i \
-e 's|^\s*\(server.modules\s*=\s*(\)$|\1\n"mod_openssl",|' \
-e 's|^#\s*\(ssl\.engine\s*=\).*$|\1"enable"|' \
-e "s|^#\\s*\\(ssl\\.pemfile\\s*=\\).*\$|\\1\"${ROUNDCUBEMAIL_HOME}/roundcubemail-server-certkey.pem\"\\nssl.ca-file=\"${ROUNDCUBEMAIL_HOME}/roundcubemail-server-cacert.pem\"|" \
/etc/lighttpd/lighttpd.conf; \
sed -i \
-e "s|^\s*\(\$config\['force_https'\]\s*=\s*\)[^;]*;|\1true;|" \
${ROUNDCUBEMAIL_HOME}/roundcubemail-config.inc.php; \
else \
sed -i \
-e "s|^\s*\(\$config\['force_https'\]\s*=\s*\)[^;]*;|\1false;|" \
${ROUNDCUBEMAIL_HOME}/roundcubemail-config.inc.php; \
fi

EXPOSE 80 443

ENTRYPOINT ["/usr/local/bin/roundcubemail-services-single.sh"]
CMD ["start-foreground"]

HEALTHCHECK CMD \
if [ "${ROUNDCUBEMAIL_HTTPS}" -eq 1 ]; then \
wget --ca-certificate=${ROUNDCUBEMAIL_HOME}/roundcubemail-server-cacert.pem https://localhost/ -O /dev/null; \
else \
wget http://localhost/ -O /dev/null; \
fi || exit 1
