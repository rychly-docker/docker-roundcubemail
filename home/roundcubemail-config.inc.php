<?php

$config['archive_mbox'] = 'archive';
$config['autocomplete_addressbooks'] = array('sql','BUT_FIT_FEEC');
$config['autoexpand_threads'] = 2;
$config['db_dsnw'] = 'sqlite:////home/roundcubemail/roundcubemail.db?mode=0646';
$config['default_charset'] = 'ISO-8859-2';
$config['default_host'] = 'ssl://imap.fit.vutbr.cz';
$config['default_list_mode'] = 'list';
$config['default_port'] = 993;
$config['delete_junk'] = true;
$config['des_key'] = '3uchWaVyRPxLssHCiX1WCirZ';
$config['drafts_mbox'] = 'drafts';
$config['email_dns_check'] = true;
$config['enable_installer'] = false;
$config['force_https'] = false;
$config['identities_level'] = 2;
$config['ip_check'] = true;
$config['junk_mbox'] = 'junk';
$config['language'] = 'cs_CZ';
$config['ldap_public']['BUT_FIT_FEEC'] = array(
	'name' => 'BUT FIT/FEEC',
	'hosts' => array('ldap.fit.vutbr.cz'),
	'search_base_dn' => 'dc=vutbr,dc=cz',
	'sizelimit' => 400,
	'timelimit' => 60,
	'writable' => false,
	'scope' => 'sub',
	'searchonly' => true,
	'search_fields' => array('cn','cnascii','mail'),
	'fieldmap' => array(
		'name' => 'gecos',
		'surname' => 'sn',
		'firstname' => 'givenName',
		'jobtitle' => 'title',
		'email' => 'mail:*',
		'phone:work' => 'telephoneNumber',
		'street' => 'street',
		'zipcode' => 'postalCode',
		'locality' => 'l',
		'country' => 'c',
		'organization' => 'o',
		'department' => 'ou',
		'website' => 'url',
		'notes' => 'roomNumber',
	),
	'filter' => '(objectClass=vutPerson)',
	'fuzzy_search' => true,
);
$config['log_driver'] = 'stdout';
$config['mail_domain'] = 'fit.vutbr.cz';
$config['message_show_email'] = true;
$config['message_sort_col'] = 'date';
$config['mime_types'] = dirname(__FILE__) . '/mime.types';
$config['plugins'] = array( // ls /opt/roundcubemail/plugins/ | sed "s/^\(.*\)$/\t\t'\1',/g"
	'acl',
	'additional_message_headers',
	'archive',
//	'attachment_reminder',
	'autologon',
//	'database_attachments',
//	'debug_logger',
	'emoticons',
	'enigma',
//	'example_addressbook',
//	'filesystem_attachments',
//	'help',
	'hide_blockquote',
//	'http_authentication',
	'identicon',
	'identity_select',
	'jqueryui',
//	'krb_authentication',
//	'managesieve',
	'markasjunk',
	'new_user_dialog',
	'new_user_identity',
	'newmail_notifier',
//	'password',
	'redundant_attachments',
	'show_additional_headers',
//	'squirrelmail_usercopy',
	'subscriptions_option',
	'userinfo',
	'vcard_attachments',
//	'virtuser_file',
//	'virtuser_query',
	'zipdownload'
);
$config['preview_pane_mark_read'] = 3;
$config['preview_pane'] = true;
$config['product_name'] = 'FIT VUT Webmail';
$config['refresh_interval'] = 60;
$config['reply_mode'] = 1;
$config['sent_mbox'] = 'sent';
$config['session_lifetime'] = 365*24*60;
$config['show_images'] = 1;
$config['show_sig'] = 2;
$config['skin'] = 'classic';
$config['skin_logo'] = 'https://www.fit.vutbr.cz/images/fitnewz.png';
$config['smtp_pass'] = '%p';
$config['smtp_port'] = 465;
$config['smtp_server'] = 'ssl://smtp.fit.vutbr.cz';
$config['smtp_user'] = '%u';
$config['support_url'] = 'https://email.fit.vutbr.cz/';
$config['temp_dir'] = '/tmp';
$config['trash_mbox'] = 'trash';
$config['undo_timeout'] = 15;
$config['use_infinite_scroll'] = true;
$config['useragent'] = NULL;
/* debug */
//$config['debug_level'] = 5;
//$config['sql_debug'] = true;
//$config['imap_debug'] = true;
//$config['ldap_debug'] = true;
//$config['smtp_debug'] = true;
?>
