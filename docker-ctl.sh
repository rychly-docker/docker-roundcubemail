#!/bin/sh

NAME=roundcubemail
[ -z "${IMAGE}" ] && IMAGE="rychly-docker/docker-${NAME}"
REGISTRY=registry.gitlab.com

# use LOCAL=1 for operations on local Docker images without REGISTRY prefix
if [ -n "${LOCAL}" ]; then
	IMAGE="${REGISTRY}/${IMAGE}"
fi

[ -z "${TAG_DEVEL}" ] && TAG_DEVEL=`git rev-parse --abbrev-ref HEAD 2>/dev/null || echo 'master'`
TAG_LATEST=latest
IMAGE_DEVEL="${IMAGE}:${TAG_DEVEL}"
IMAGE_LATEST="${IMAGE}:${TAG_LATEST}"

DOCKER_PORTS=`grep '\(ENV\|ARG\) [^ ]*_PORT=' $(dirname ${0})/Dockerfile | cut -d ' ' -f 2 | sed 's/^\(.*\)=\(.*\)$$/\2:\1/g' | sort -n`

ACTION="${1}"
shift
case "${ACTION}" in
clean )
	docker ps -a -f "ancestor=${IMAGE_DEVEL}" --format '{{.ID}}' | xargs -r docker rm -f -v
	exec docker rmi "${IMAGE_DEVEL}"
	;;
clean-latest )
	docker ps -a -f "ancestor=${IMAGE_LATEST}" --format '{{.ID}}' | xargs -r docker rm -f -v
	exec docker rmi "${IMAGE_LATEST}"
	;;
build )
	exec docker build --pull -t "${IMAGE_DEVEL}" .
	;;
deploy )
	echo "TODO: deploy"
	;;
desc-ports )
	echo "${DOCKER_PORTS}" | tr ' ' '\n'
	;;
pull-devel )
	exec docker pull "${IMAGE_DEVEL}"
	;;
push-devel )
	exec docker push "${IMAGE_DEVEL}"
	;;
pull-latest )
	exec docker pull "${IMAGE_LATEST}"
	;;
push-latest )
	exec docker push "${IMAGE_LATEST}"
	;;
tag-latest )
	exec docker tag "${IMAGE_DEVEL}" "${IMAGE_LATEST}"
	;;
show-ports )
	docker ps -f "ancestor=${IMAGE_DEVEL}" -f "ancestor=${IMAGE_LATEST}" --format '{{.ID}}' \
	| exec xargs -n 1 docker port
	;;
show-ips )
	docker ps -f "ancestor=${IMAGE_DEVEL}" -f "ancestor=${IMAGE_LATEST}" --format '{{.ID}}' \
	| exec xargs -n 1 docker inspect | grep '\("Id"\|"Hostname"\|"Image"\|"IPAddress"\)'
	;;
start* )
	IMGNAME="${NAME}-${ACTION#start}"
	docker start "${IMGNAME}" \
	|| exec docker run --init --detach --publish-all "--hostname=${IMGNAME}" "--name=${IMGNAME}" $@ "${IMAGE_DEVEL}"
	;;
start-latest* )
	IMGNAME="${NAME}-${ACTION#start-latest}"
	docker start "${IMGNAME}" \
	|| exec docker run --init --detach --publish-all "--hostname=${IMGNAME}" "--name=${IMGNAME}" $@ "${IMAGE_LATEST}"
	;;
shell* )
	IMGNAME="${NAME}-${ACTION#shell}"
	docker start --interactive "${IMGNAME}" \
	|| exec docker run --init --tty --interactive --publish-all "--hostname=${IMGNAME}" "--name=${IMGNAME}" --entrypoint="" $@ "${IMAGE_DEVEL}" /bin/sh --login
	;;
shell-latest* )
	IMGNAME="${NAME}-${ACTION#shell-latest}"
	docker start --interactive "${IMGNAME}" \
	|| exec docker run --init --tty --interactive --publish-all "--hostname=${IMGNAME}" "--name=${IMGNAME}" --entrypoint="" $@ "${IMAGE_LATEST}" /bin/sh --login
	;;
test* )
	IMGNAME="${NAME}-${ACTION}"
	docker start --interactive "${IMGNAME}" \
	|| exec docker run "--hostname=${IMGNAME}" "--name=${IMGNAME}" $@ "${IMAGE_DEVEL}" "/usr/local/bin/${IMGNAME}.sh"
	;;
stop* )
	IMGNAME="${NAME}-${ACTION#stop}"
	exec docker stop "${IMGNAME}"
	;;
stop-latest* )
	IMGNAME="${NAME}-${ACTION#stop-latest}"
	exec docker stop "${IMGNAME}"
	;;
remove* )
	IMGNAME="${NAME}-${ACTION#remove}"
	exec docker rm -f -v "${IMGNAME}"
	;;
remove-latest* )
	IMGNAME="${NAME}-${ACTION#remove-latest}"
	exec docker rm -f -v "${IMGNAME}"
	;;
*)
	echo "Unknown action '${ACTION}'!" >&2
	echo "For actions test, shell/start/stop/remove, and shell/start/stop/remove-latest, there must be a suffix after the action name to run the image in a particular instance, e.g., as 'test-myinstance'." >&2
	exit 1
esac
