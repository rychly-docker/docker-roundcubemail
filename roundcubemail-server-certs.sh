#!/bin/sh

BASENAME=${0%certs.sh}
OUTPUT_CA=home/$(basename ${BASENAME})cacert.pem
OUTPUT_CK=home/$(basename ${BASENAME})certkey.pem

openssl req -x509 -sha256 -newkey rsa:2048 -nodes -keyout "${OUTPUT_CK}" -out "${OUTPUT_CA}" -days 3650 -subj '/CN=localhost'
cat "${OUTPUT_CA}" >> "${OUTPUT_CK}"
